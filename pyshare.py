#!/usr/bin/python3

import json, requests, time, traceback, re

url = "http://127.0.0.1:9090/api/v2"
chatName = 'sehraf'

stateTokens = []
lobbies = {}

debug = False

prefix = '[BOT]'
rules = [
			{
				'match': re.compile('^ping$', re.IGNORECASE),
				'response': 'pong'
			},
			{
				'match': re.compile('^test$', re.IGNORECASE),
				'response': '@__nick__: test back :P'
			},
			{
				'match': re.compile('^echo', re.IGNORECASE),
				'response': '@__nick__<br>> __msg__'
			},
			{
				'match': re.compile('(^|\s)Lua4RS($|\s)', re.IGNORECASE),
				'response': '\o/'
			},
			{
				'match': re.compile('^!links$'),
				'response': 'Please visit:<br>http://retroshare.org<br>http://redd.it/18vsq5<br>https://github.com/RetroShare/RetroShare<br>unofficial user wiki: http://retroshare.wikidot.com<br>DevBlog: http://retroshareteam.wordpress.com<br>public chatservers https://retroshare.rocks/'
			},
			{
				'match': re.compile('^!key$'),
				'response': 'chatwindow > right mouse > paste cert link'
			},
			{
				'match': re.compile('^!info'),
				'response': 'I\'m just a python script that uses the json API!'
			},
		]

def sendRequest(sector, function = "", data1 = None, data2 = None, data1IsPath = False):
	url2 = url + '/' + sector
	if function != "":
		url2 += '/' + function
	if data1IsPath:
		url2 += '/' + data1
		data = data2
	else:
		data = data1

	debugDump('POST: ' + url2, data)
	resp = requests.post(url=url2, json=data)
	debugDump('RESP', resp.json())
	return resp.json()

'''
api calls
'''
def getChatLobbies():
	return sendRequest('chat', 'lobbies')

def getChatLobbyInfo(lobbyId):
	return sendRequest('chat', 'info', lobbyId, None, True)

def getChatMessages(lobbyId):
	req = {'begin_after': lobbies[lobbyId]['last_id']}
	resp = sendRequest('chat', 'messages', lobbyId, req, True)
	#addStateToken(resp)
	return resp

def getChatMessagesMarkAsRead(lobbyId):
	return sendRequest('chat', 'mark_chat_as_read', lobbyId, None, True)

def getChatUnreadMessages():
	resp = sendRequest('chat', 'unread_msgs')
	addStateToken(resp)
	return resp

def getChatSendMessage(chatId, msg):
	req = {'chat_id': chatId, 'msg': msg}
	return sendRequest('chat', 'send_message', req)

def getChatSubscribe(lobbyId, gxsId):
	req = {'id': lobbyId, 'gxs_id': gxsId}
	return sendRequest('chat', 'subscribe_lobby', req)

def getStateToken():
	return sendRequest('statetokenservice', "", stateTokens)

def getOwnIds():
	return sendRequest('identity', 'own')

'''
json
'''
# currently not used
# def getReturnCode(data):
# 	return data['returncode']

def getReturnDebugMsg(data):
	return data['debug_msg']

def getItemNumber(data):
	return len(data['data'])

def getStateTokenFromJson(data):
	if 'statetoken' in data:
		return data['statetoken']
	else:
		return None

def debugDump(label, data):
	if not debug: return
	print(label, json.dumps(data, sort_keys=True, indent=4))

'''
others
'''
def updateTokens():
	resp = getStateToken()

	oldTokens = []
	if resp['data']:
		for token in resp['data']:
			stateTokens.remove(token)
			oldTokens.append(token)
		return True, oldTokens
	return False, None

def addStateToken(resp):
	token = getStateTokenFromJson(resp)
	if token != None:
		stateTokens.append(token)
	else:
		# no statetoken was set
		# show stack trace to find out what call was send
		for line in traceback.format_stack():
			print(line.strip())

def printChat(data, chatId):
	print()
	print(lobbies[chatId]['name'] + ':')

	for i in range(getItemNumber(data)):
		lobbies[chatId]['last_id'] = data['data'][i]['id']
		print(data['data'][i]['author_name'] + ':', data['data'][i]['msg'])

'''
bot
'''
def process(jsonMsg, chatId):
	msg = jsonMsg['msg']
	author = jsonMsg['author_name']

	# eigene Antworten ignorieren
	if(msg.startswith(prefix)):
		return

	# if msg == 'py-ping':
	# 	resp = getChatSendMessage(chatId, '[Pyjon]: pong')
	# 	debugDump('answering', resp)
	for i in range(len(rules)):
		if rules[i]['match'].search(msg) != None:
			# build answer
			resp = prefix + ' ' + rules[i]['response']
			# replace markers
			resp = resp.replace('__nick__', author)
			resp = resp.replace('__msg__', msg)
			# send message
			resp = getChatSendMessage(chatId, resp)

'''
program
'''
def showUnread():
	# unread = getChatUnreadMessages()
	# debugDump('unread', unread)
	# for i in range(getItemNumber(unread)):
	for id in lobbies:
		#id = unread['data'][i]['id']

		# only process chat lobbies for now
		if id[0] != 'L' and id[0] != 'B':
			continue

		resp = getChatMessages(id)

		if getItemNumber(resp) == 0:
			continue

		printChat(resp, id)

		for i in range(getItemNumber(resp)):
			process(resp['data'][i], id)

		resp = getChatMessagesMarkAsRead(id)

def addNewLobby(elem):
	lobby = {}
	lobby['id']			= elem['id']
	lobby['chat_id']	= elem['chat_id']
	lobby['name']		= elem['name']
	lobby['last_id']	= '0'

	chatId = elem['chat_id']
	lobbies[chatId] = lobby

	# load all old messages	
	while True:
		resp = getChatMessages(chatId)
		printChat(resp, chatId)

		if getReturnDebugMsg(resp) != "limited the number of returned items to 20\n":
			break

	# mark all messages as read
	getChatMessagesMarkAsRead(chatId)


# list IDs and select based on given name
print('searching gxs id for name "' + chatName + '"')
gxs_id = ""

ids = getOwnIds()
# if getReturnCode(ids) == 'ok':
for i in range(getItemNumber(ids)):
	print(ids['data'][i]['name'], ids['data'][i]['gxs_id'])
	if ids['data'][i]['name'] == chatName:
		gxs_id = ids['data'][i]['gxs_id']

print('using gxs id: "' + gxs_id + '"')

# load lists and old messages
l = getChatLobbies()
for i in range(getItemNumber(l)):
	elem = l['data'][i]
	if elem['subscribed']:
		addNewLobby(elem)

# call once to get token
getChatUnreadMessages()

# dive into loop
while True:
	r, t = updateTokens()
	if r:
		showUnread()
		# set state token for unread messages
		getChatUnreadMessages()
		#print(t)
	# else:
	# 	print('-')
	#print(len(stateTokens))
	time.sleep(2)
