#!/usr/bin/python3

import json, requests, time, re #, traceback

# The send wrapper is only used to have a central point to interact with message sending/receiving (e.g. handle state tokens)
# Currently the wrapper is pretta dump and just "redirects" the call to RS

chatName = 'sehraf'
debug = False

def debugDump(label, data):
	if not debug: return
	print(label, json.dumps(data, sort_keys=True, indent=4))

def getItemNumber(data):
	return len(data['data'])

def getReturnDebugMsg(data):
	return data['debug_msg']

def printChat(data, lobby):
	print()
	print(lobby.getName() + ':')

	for i in range(getItemNumber(data)):
		lobby.setLastId(data['data'][i]['id'])
		print(' - ' + data['data'][i]['author_name'] + ':', data['data'][i]['msg'])



class rsAPI:
	''' static '''
	url = "http://127.0.0.1:9090/api/v2/"

	def sendRequest(self, function, data = None):
		url2 = self.url + function

		debugDump('POST: ' + url2, data)
		resp = requests.post(url=url2, json=data)

		debugDump('RESP', resp.json())
		return resp.json()

	def getStateToken(self, tokens):
		return self.sendRequest('statetokenservice', tokens)




class rsLobby:
	def __init__(self, api, elem):
		#debugDump('init rsLobby', elem)
		self.api    = api
		self.id		= elem['id']
		self.chatId	= elem['chat_id']
		self.name	= elem['name']
		self.lastId	= '0'

	''' send wrapper '''
	def sendRequest(self, function, data = None):
		#addStateToken(resp)
		return self.api.sendRequest(function, data)

	def getId(self):
		return self.id

	def getChatId(self):
		return self.chatId	

	def getName(self):
		return self.name	

	def getLastId(self):
		return self.lastId

	def setLastId(self, id):
		self.lastId = id

	def markAllChatMessagesMarkAsRead(self):
		# load all old messages	
		while True:
			resp = self.getChatMessages()
			printChat(resp, self)

			if getReturnDebugMsg(resp) != "limited the number of returned items to 20\n":
				break

		# mark all messages as read
		self.getChatMessagesMarkAsRead()

	def getChatLobbyInfo(self):
	 	return self.sendRequest('chat/info/' + self.chatId)

	def getChatMessages(self):
		req = {'begin_after': self.lastId}
		resp = self.sendRequest('chat/messages/' + self.chatId, req)		
		return resp

	def getChatMessagesMarkAsRead(self):
		return self.sendRequest('chat/mark_chat_as_read/' + self.chatId)



class rsChat:
	def __init__(self, bot, gxsId):
		self.gxsId = gxsId
		self.lobbies = {}
		self.bot = bot

	''' send wrapper '''
	def sendRequest(self, function, data = None):
		#addStateToken(resp)
		return self.bot.getAPI().sendRequest(function, data)

	# funtions
	def getAllChatLobbies(self):
		# load lists and old messages
		l = self.getChatLobbies()
		for i in range(getItemNumber(l)):
			elem = l['data'][i]
			if elem['subscribed']:
				debugDump('adding lobby', elem)

				lobby = rsLobby(self.bot.getAPI(), elem)
				# add lobby first to list for other funtions
				self.lobbies[lobby.getChatId()] = lobby

				#lobby.markAllChatMessagesMarkAsRead()
		return self.lobbies

	def getChatLobby(self, lobbyId):
		return self.lobbies[lobbyId]

	def processUnread(self):
		# unread = getChatUnreadMessages()
		# debugDump('unread', unread)
		# for i in range(getItemNumber(unread)):
		for id, lobby in self.lobbies.items():
			#id = lobby.getChatId()

			# only process chat lobbies for now
			if id[0] != 'L': #and id[0] != 'B':
				continue

			resp = lobby.getChatMessages()

			if getItemNumber(resp) == 0:
				continue

			printChat(resp, lobby)

			for i in range(getItemNumber(resp)):
				self.bot.botProcess(resp['data'][i], id)

			resp = lobby.getChatMessagesMarkAsRead()

	# API calls
	def getChatLobbies(self):
		return self.sendRequest('chat/lobbies')

	def getChatSubscribe(self, lobbyId, gxsId):
		req = {'id': lobbyId, 'gxs_id': gxsId}
		return self.sendRequest('chat/subscribe_lobby', req)

	def getChatUnreadMessages(self):
		resp = self.sendRequest('chat/unread_msgs')
		return resp

	def getChatSendMessage(self, chatId, msg):
		req = {'chat_id': chatId, 'msg': msg}
		return self.sendRequest('chat/send_message', req)



class rsPeers:
	def __init__(self, api):
		self.rs = api

	''' send wrapper '''
	def sendRequest(self, function, data = None):
		#addStateToken(resp)
		return self.rs.sendRequest(function, data)

	def getOwnIds(self):
		return self.sendRequest('identity/own')

	def getGXSIdByName(self, name):
		ids = self.getOwnIds()
		# if getReturnCode(ids) == 'ok':
		for i in range(getItemNumber(ids)):
			#print(ids['data'][i]['name'], ids['data'][i]['gxs_id'])
			if ids['data'][i]['name'] == name:
				return ids['data'][i]['gxs_id']

class rsBot:
	def __init__(self, name):
		self.api = rsAPI()
		self.prefix = '[Pyshare]'
		self.rules = [
				{
					'match': re.compile('^ping$', re.IGNORECASE),
					'response': 'pong'
				},
				{
					'match': re.compile('^test$', re.IGNORECASE),
					'response': '@__nick__: test back :P'
				},
				{
					'match': re.compile('^echo', re.IGNORECASE),
					'response': '@__nick__<br>> __msg__'
				},
				{
					'match': re.compile('(^|\s)Lua4RS($|\s)', re.IGNORECASE),
					'response': '\o/'
				},
				{
					'match': re.compile('^!links$'),
					'response': 'Please visit:<br>http://retroshare.org<br>http://redd.it/18vsq5<br>https://github.com/RetroShare/RetroShare<br>unofficial user wiki: http://retroshare.wikidot.com<br>DevBlog: http://retroshareteam.wordpress.com<br>public chatservers https://retroshare.rocks/'
				},
				{
					'match': re.compile('^!key$'),
					'response': 'chatwindow > right mouse > paste cert link'
				},
				{
					'match': re.compile('^!info'),
					'response': 'I\'m just a python script that uses the json API!'
				},
			]

		self.botInit(name)

	def run(self):
		while True:
			t = []
			t.append(self.tokenUnread)
			r = self.api.getStateToken(t)

			if self.tokenUnread in r['data']:
				self.chat.processUnread()
				t = self.chat.getChatUnreadMessages()
				self.tokenUnread = t['statetoken']

			time.sleep(2)

	def botInit(self, name):
		# list IDs and select based on given name
		peers = rsPeers(self.api)
		print('searching gxs id for name "' + name + '"')
		gxs_id = peers.getGXSIdByName(name)
		print('using gxs id: "' + gxs_id + '"')

		self.chat = rsChat(self, gxs_id)

		# load lists and old messages
		lobbies = self.chat.getAllChatLobbies()
		for lobby in lobbies:
			self.chat.getChatLobby(lobby).markAllChatMessagesMarkAsRead()

		# get token
		t = self.chat.getChatUnreadMessages()
		self.tokenUnread = t['statetoken']

	def botProcess(self, jsonMsg, chatId):
		msg = jsonMsg['msg']
		author = jsonMsg['author_name']

		# ignore own answers
		if(msg.startswith(self.prefix)):
			return

		for i in range(len(self.rules)):
			if self.rules[i]['match'].search(msg) != None:
				# build answer
				resp = self.prefix + ' ' + self.rules[i]['response']
				# replace markers
				resp = resp.replace('__nick__', author)
				resp = resp.replace('__msg__', msg)
				# send message
				resp = self.chat.getChatSendMessage(chatId, resp)

	def getAPI(self):
		return self.api



if __name__ == "__main__":
	bot = rsBot(chatName)
	bot.run()