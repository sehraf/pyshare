#!/usr/bin/python3

import json, requests, time, re #, traceback

chatName = 'sehraf'
debug = False

def debugDump(label, data):
	if not debug: return
	print(label, json.dumps(data, sort_keys=True, indent=4))

def printChat(data, lobby):
	print()
	print(lobby.getName() + ':')

	for i in range(getItemNumber(data)):
		lobby.setLastId(data['data'][i]['id'])
		print(' - ' + data['data'][i]['author_name'] + ':', data['data'][i]['msg'])

def sendRequest(function, data = None):
	url = "http://127.0.0.1:9092/" + function

	debugDump('POST: ' + url, data)
	resp = requests.post(url=url, json=data)

	debugDump('RESP', resp.json())
	return resp.json()

def valid(response):
	return response['retval']

def uintToLobbyID(uint):
	return 'L{0:0{1}X}'.format(uint,16)

def getItemNumber(data):
	return len(data['data'])




class rsAPI:
	''' static '''
	url = "http://127.0.0.1:9090/api/v2/"

	def sendRequest(self, function, data = None):
		url2 = self.url + function

		debugDump('POST: ' + url2, data)
		resp = requests.post(url=url2, json=data)

		debugDump('RESP', resp.json())
		return resp.json()

	def getStateToken(self, tokens):
		return self.sendRequest('statetokenservice', tokens)



class rsLobby:
	def __init__(self, api, id):
		self.api = api

		req = {"id": id}
		resp = sendRequest('rsMsgs/getChatLobbyInfo', req)
		if valid(resp):
			elem = resp['info']

			self.chatId	= elem['lobby_id']
			self.name	= elem['lobby_name']
		
		self.lastId	= '0'

	def getChatId(self):
		return self.chatId	

	def getName(self):
		return self.name	

	def getLastId(self):
		return self.lastId

	def setLastId(self, id):
		self.lastId = id

	def markAllChatMessagesMarkAsRead(self):
		# load all old messages	
		while True:
			resp = self.getChatMessages()
			printChat(resp, self)

			if resp['debug_msg'] != "limited the number of returned items to 20\n":
				break

		# mark all messages as read
		self.getChatMessagesMarkAsRead()

	def getChatMessages(self):
		req = {'begin_after': self.lastId}
		resp = self.api.sendRequest('chat/messages/' + uintToLobbyID(self.chatId), req)		
		return resp

	def getChatMessagesMarkAsRead(self):
		return self.api.sendRequest('chat/mark_chat_as_read/' + uintToLobbyID(self.chatId))



class rsChat:
	def __init__(self, bot):
		self.lobbies = {}
		self.bot = bot

	# funtions
	def getAllChatLobbies(self, markAsRead = False):
		# load lists and old messages
		lobbieIDs = self.getChatLobbies()
		for lobbyID in lobbieIDs:				
			debugDump('adding lobby', lobbyID)

			l = rsLobby(self.bot.getAPI(), lobbyID)
			# add lobby first to list for other funtions
			self.lobbies[l.getChatId()] = l

			if markAsRead:
				l.markAllChatMessagesMarkAsRead()

		return self.lobbies

	def getChatLobby(self, lobbyId):
		return self.lobbies[lobbyId]

	def processUnread(self):
		# unread = getChatUnreadMessages()
		# debugDump('unread', unread)
		# for i in range(getItemNumber(unread)):
		for id, lobby in self.lobbies.items():
			resp = lobby.getChatMessages()

			if getItemNumber(resp) == 0:
				continue

			printChat(resp, lobby)

			for i in range(getItemNumber(resp)):
				self.bot.botProcess(resp['data'][i], id)

			resp = lobby.getChatMessagesMarkAsRead()

	# API calls
	def getChatLobbies(self):
		return sendRequest('rsMsgs/getChatLobbyList')['cl_list']

	def getChatUnreadMessages(self):
		resp = self.bot.getAPI().sendRequest('chat/unread_msgs')
		return resp

	def getChatSendMessage(self, chatId, msg):
		req = {'id': {'type': 3, 'lobby_id': chatId}, 'msg': msg}
		return self.bot.getAPI().sendRequest('rsMsgs/sendChat', req)



class rsBot:
	def __init__(self):
		self.api = rsAPI()
		self.prefix = '[Pyshare+jsonapi]'
		self.rules = [
				{
					'match': re.compile('^ping$', re.IGNORECASE),
					'response': 'pong'
				},
				{
					'match': re.compile('^test$', re.IGNORECASE),
					'response': '@__nick__: test back :P'
				},
				{
					'match': re.compile('^echo', re.IGNORECASE),
					'response': '@__nick__<br>> __msg__'
				},
				{
					'match': re.compile('(^|\s)Lua4RS($|\s)', re.IGNORECASE),
					'response': '\o/'
				},
				{
					'match': re.compile('^!links$'),
					'response': 'Please visit:<br>http://retroshare.org<br>http://redd.it/18vsq5<br>https://github.com/RetroShare/RetroShare<br>unofficial user wiki: http://retroshare.wikidot.com<br>DevBlog: http://retroshareteam.wordpress.com<br>public chatservers https://retroshare.rocks/'
				},
				{
					'match': re.compile('^!key$'),
					'response': 'chatwindow > right mouse > paste cert link'
				},
				{
					'match': re.compile('^!info'),
					'response': 'I\'m just a python script that uses the json API!'
				},
			]

		self.botInit()

	def run(self):
		while True:
			t = []
			t.append(self.tokenUnread)
			r = self.api.getStateToken(t)

			if self.tokenUnread in r['data']:
				self.chat.processUnread()
				t = self.chat.getChatUnreadMessages()
				self.tokenUnread = t['statetoken']

			time.sleep(2)

	def botInit(self):
		self.chat = rsChat(self)

		# load lists and mark old messages
		self.chat.getAllChatLobbies(True)

		# get token
		t = self.chat.getChatUnreadMessages()
		self.tokenUnread = t['statetoken']

	def botProcess(self, jsonMsg, chatId):
		msg = jsonMsg['msg']
		author = jsonMsg['author_name']

		# ignore own answers
		if(msg.startswith(self.prefix)):
			return

		for i in range(len(self.rules)):
			if self.rules[i]['match'].search(msg) != None:
				# build answer
				resp = self.prefix + ' ' + self.rules[i]['response']
				# replace markers
				resp = resp.replace('__nick__', author)
				resp = resp.replace('__msg__', msg)
				# send message
				resp = self.chat.getChatSendMessage(chatId, resp)

	def getAPI(self):
		return self.api



if __name__ == "__main__":
	bot = rsBot()
	bot.run()